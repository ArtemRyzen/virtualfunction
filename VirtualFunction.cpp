// VirtualFunction.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>

using namespace std;

class Wheel
{
public:
	Wheel()
	{
		Diameter = 0.f;
	}
	
	Wheel(float Diameter)
	{
		this->Diameter = Diameter;
	}

	float GetDiameter() const
	{
		return Diameter;
	}
	
private:
	float Diameter;
};

class Engine
{
public:
	Engine()
	{
		Power = 0.f;
	}
	
	Engine(float Power)
	{
		this->Power = Power;
	}

	float GetPower() const
	{
		return Power;
	}
	
private:
	float Power;
};

class Vehicle
{
public:
	virtual ostream& Print(ostream& out) const
	{
		out << "Vehile";
		return out;
	};

	friend ostream& operator<<(ostream& out, const Vehicle& Class)
	{
		return Class.Print(out);
	}

	virtual ~Vehicle(){};
};

class WaterVehicle : public Vehicle
{
public:
	WaterVehicle(float Draught = 0.f)
	{
		Draught = 0.f;
	}

	ostream& Print(ostream& out) const override
	{
		out << "Draught: " << Draught;
		return out;
	}
	
protected:
	float Draught = 0.f;
};

class RoadVehicle : public Vehicle
{
protected:
	float RideHeight = 0.f;
};

class Bicycle : public RoadVehicle
{
public:
	Bicycle(Wheel W1, Wheel W2, float GroundClearance)
	{
		this->RideHeight = GroundClearance;
		this->W1 = W1;
		this->W2 = W2;
	}

	ostream& Print(ostream& out) const override
	{
		out << "Bicycle Wheels: " << W1.GetDiameter() << " " << W2.GetDiameter() << "  Ride height: " << RideHeight;
		return out;
	}
	
private:
	Wheel W1;
	Wheel W2;
};

class Car : public RoadVehicle
{
public:
	Car(Engine Engine, Wheel W1, Wheel W2, Wheel W3, Wheel W4, float GroundClearance)
	{
		EngineCar = Engine;
		this->W1 = W1;
		this->W2 = W2;
		this->W3 = W3;
		this->W4 = W4;
		this->RideHeight = GroundClearance;
	}	

	ostream& Print(ostream& out) const override
	{
		out << "EngineCar: " << EngineCar.GetPower() << " Wheels: " << W1.GetDiameter() << " " << W2.GetDiameter() << " " << W3.GetDiameter() << " " << W4.GetDiameter() << " Ride height: " << RideHeight;
		return out;
	}

	Engine GetEngine()
	{
		return EngineCar;
	}
	
private:
	Engine EngineCar;
	
	Wheel W1;
	Wheel W2;
	Wheel W3;
	Wheel W4;
};

float getHighestPower(vector<Vehicle*> v)
{
	float lastPower = 0.f;
	
	if(v.size() <= 0.f)
	{
		return 0.f;
	}
	
	for (int x = 0; x < v.size(); x++)
	{
		Car* car = dynamic_cast<Car*>(v[x]);
		if(car)
		{
			if(lastPower < car->GetEngine().GetPower())
			{
				lastPower = car->GetEngine().GetPower();
			}
		}
	}

	return lastPower;
}

int main()
{
	//Chapter 1
	Car c(Engine(150.f), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150.f);
	cout << c << endl;

	Bicycle t(Wheel(20), Wheel(20), 300.f);
	cout << t << '\n' << endl;

	//Chapter 2
	vector<Vehicle*> v;
	v.push_back(new Car(Engine(400.f), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150.f));
	v.push_back(new Car(Engine(150.f), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150.f));
	v.push_back(new Car(Engine(200.f), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130.f));
	v.push_back(new WaterVehicle(5000.f));

	//Output vector
	for(int x = 0; x < v.size() ; x++)
	{
		cout << *v[x] << '\n';
	}

	std::cout << "The highest power is " << getHighestPower(v) << '\n'; // ���������� ��� �������
	
	for(int x = 0; x<v.size();x++)
	{
		delete v[x];
	}
	
	v.clear();
}
	